## 一、支持平台

| App    | H5  | 微信小程序 | 支付宝小程序 | 百度小程序 | 字节跳动小程序 | QQ 小程序 |
| ------ | --- | ---------- | ------------ | ---------- | -------------- | --------- |
| 未测试 | √   | √          | √            | √          | √              | √         |

## 二、效果演示

<table>
<tr>
    <th>兼容不同屏幕大小</th>
    <th>绘制图形，图片，文本等展示</th>
  </tr>
  <tr>
    <td><img src="https://huangchunhongzz.gitee.io/imgs/poster/poster_gif.gif" height = '400' /></td>
    <td><img src="https://huangchunhongzz.gitee.io/imgs/poster/poster_gif_20210730.gif" height = '400'  /></td>
  </tr>
</table>

## 三、注意事项

### 1、小程序下获取网络图片信息需先配置 downloadFile 域名白名单才能生效。否则会报错（downloadFile 合法域名校验失败），并且绘制海报失败

流程：开发平台->服务器域名->uploadFile 合法域名要添加上 https://huangchunhongzz.gitee.io 否则没办法下载成功图片

### 2、hch-poster 海报模板，主要按照苹果 375\*667 比例，其他型号手机等比例缩放绘制海报，以达到兼容不同屏幕大小的效果

### 3、微信小程序要在基础库 2.16.0 及以下，否则会报（Uncaught TypeError: Cannot delete property 'WeixinJSBridge' of #<Window>）页面空白现象

### 4、H5 需要注意使用的图片要支持跨域（插件例子的图片目前是不支持跨域的，可以自行替换成支持跨域的图片，已测试可行性）

## 四、海报模板使用方法（主要看 pages/hch-poster 页面的例子里面有详细说明）

```js

<hch-poster ref="hchPoster" @cancel="handleCancel" :posterData.sync="posterData" @previewImage='previewImage' />

import HchPoster from "../../components/hch-poster/hch-poster.vue"

export default {
  components: {
    HchPoster
  },
  data() {
    return {
      // 海报模板数据
      posterData: {
        poster: {
          //根据屏幕大小自动生成海报背景大小
          url: "https://huangchunhongzz.gitee.io/imgs/poster/poster_bg_3.png",//图片地址
          r: 10, //圆角半径
          w: 300, //海报宽度
          h: 480, //海报高度
          p: 20 //海报内边距padding
        },
        mainImg: {
          //海报主商品图
          url: "https://huangchunhongzz.gitee.io/imgs/poster/product.png",//图片地址
          r: 10, //圆角半径
          w: 250, //宽度
          h: 200 //高度
        },
        title: {
          //商品标题
          text: "今日上新水果，牛奶草莓，颗粒饱满，每盒 200g",//文本
          fontSize: 16, //字体大小
          color: "#000", //颜色
          lineHeight: 25, //行高
          mt: 20 //margin-top
        },
        codeImg: {
          //小程序码
          url: "https://huangchunhongzz.gitee.io/imgs/poster/code.png",//图片地址
          w: 100, //宽度
          h: 100, //高度
          mt: 20, //margin-top
          r: 50 //圆角半径
        },
        tips: [
          //提示信息
          {
            text: "记忆之王",//文本
            fontSize: 14,//字体大小
            color: "#2f1709",//字体颜色
            align: "center",//对齐方式
            lineHeight: 25,//行高
            mt: 20//margin-top
          },
          {
            text: "长按/扫描识别查看商品",//文本
            fontSize: 12,//字体大小
            color: "#2f1709",//字体颜色
            align: "center",//对齐方式
            lineHeight: 25,//行高
            mt: 20//margin-top
          }
        ]
      }
    }
  }
}

```

## 五、API

### 生成海报的方法调用如

```js
this.$refs.hchPoster.posterShow()
```

### 取消海报

```js
//有$emit方法
this.$emit('cancel', true)
// 所以在组件中用@cancel绑定方法适用
// <hch-poster ref="hchPoster" @cancel="handleCancel" :posterData.sync="posterData" @previewImage='previewImage' />
```

### 海报预览（H5 端才有的方法）

```js
//有$emit方法
this.$emit('previewImage', base64)
// 所以在组件中用@previewImage绑定方法适用
// <hch-poster ref="hchPoster" @cancel="handleCancel" :posterData.sync="posterData" @previewImage='previewImage' />
```

## 六、props

| name | 描述 | 默认值 | 类型 |
| --- | --- | --- | --- |
| posterData | 海报的属性(修改海报的属性值就可以生成自己的海报) | (具体数据参考上方，posterData:{}) | Object |

## 七、开源不易，喜欢的请 star

### 喜欢这个海报，觉得好用的，可以支持打赏下哦

<table>
<img src="https://gitee.com/huangchunhongzZ/imgs/raw/master/poster/weixin.jpg" height = '200' style='height:400px;'  />
</table>
